import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

public class Product{
	WebDriver _driver;
	WebDriverWait _wait;

	public Product(WebDriver driver, WebDriverWait wait)
	{
		_driver = driver;
		_wait = wait;
	}

	By _addToCart = By.name("Submit");
	By _proceedToCheckout = By.linkText("Proceed to checkout");

	public void SelectAddToCart()
	{
		_wait.until(ExpectedConditions.visibilityOfElementLocated(_addToCart)).click();
	}

	public void ProceedToCheckout()
	{
		_wait.until(ExpectedConditions.visibilityOfElementLocated(_proceedToCheckout)).click();
        _wait.until(ExpectedConditions.visibilityOfElementLocated(_proceedToCheckout)).click();
	}
}