import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

import Login;

public class CreateAccount{

	WebDriver _driver;
	WebDriverWait _wait;

	public CreateAccount(WebDriver driver, WebDriverWait wait)
	{
		_driver = driver;
		_wait = wait;
	}

	//Elements
	By _emailCreate = By.id("email_create");
	By _submitCreate = By.id("SubmitCreate");
	By _genderTwo = By.id("id_gender2");
	By _customerFirstName = By.id("customer_firstname");
	By _customerLastName = By.id("customer_lastname");
	By _company = By.id("company");
	By _addressOne = By.id("address1");
	By _addressTwo = By.id("address2");
	By _city = By.id("city");
	By _stateSelect = By.id("id_state");
	By _daysSelect = By.id("days");
	By _monthSelect = By.id("months");
	By _yearsSelect = By.id("years");
	By _postcode = By.id("postcode");
	By _other = By.id("other");
	By _phone = By.id("phone");
	By _phoneMobile = By.id("phone_mobile");
	By _alias = By.id("alias");
	By _submitAccount = By.id("submitAccount");

	public void CreateNewUserAccount(String email, String firstName, String lastName, String password)
	{
		Login login = new Login(_driver, _wait);

		//Submit Email
        _driver.findElement(_emailCreate).sendKeys(email);
        _driver.findElement(_submitCreate).click();

        //Personal Information
        _wait.until(ExpectedConditions.visibilityOfElementLocated(_genderTwo)).click();
        _driver.findElement(_customerFirstName).sendKeys(firstName);
        _driver.findElement(_customerLastName).sendKeys(lastName);
        _driver.findElement(login._password).sendKeys(password);

        //DOB
        Select select = new Select(_driver.findElement(_daysSelect));
        select.selectByValue("1");
        select = new Select(_driver.findElement(_monthSelect));
        select.selectByValue("1");
        select = new Select(_driver.findElement(_yearsSelect));
        select.selectByValue("2000");

        //Address
        _driver.findElement(_company).sendKeys("Company");
        _driver.findElement(_addressOne).sendKeys("Qwerty, 123");
        _driver.findElement(_addressTwo).sendKeys("zxcvb");
        _driver.findElement(_city).sendKeys("Qwerty");
        select = new Select(_driver.findElement(_stateSelect));
        select.selectByVisibleText("Colorado");
        _driver.findElement(_postcode).sendKeys("12345");
        _driver.findElement(_other).sendKeys("Qwerty");
        _driver.findElement(_phone).sendKeys("12345123123");
        _driver.findElement(_phoneMobile).sendKeys("12345123123");
        _driver.findElement(_alias).sendKeys("hf");
        _driver.findElement(_submitAccount).click();
	}
}