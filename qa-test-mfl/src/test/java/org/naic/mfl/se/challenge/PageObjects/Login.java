import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;

public class Login {

	WebDriver _driver;
	WebDriverWait _wait;

	public Login(WebDriver driver, WebDriverWait wait)
	{
		_driver = driver;
		_wait = wait;
	}

	//By Elements
	By _login = By.classname("login");
	By _email = By.id("email");
	By _password = By.id("passwd");
	By _signIn = By.id("SubmitLogin");
	By _accountName = By.classname("account");
	By _header = By.cssSelector("h1");
	By _infoAccount = By.className("info-account");
	By _logOut = By.className("logout");

	public void ClickLogin()
	{
		_wait.until(ExpectedConditions.visibilityOfElementLocated(_login)).click();
	}

	public void SignInToSite(String email, String password)
	{
		//email
		_driver.findElement(_email).sendKeys(email);
		//password
		_driver.findElement(_password).sendKeys(password);
		//submit
		_driver.findElement(_signIn).click();
	}

	public void VerifyLogin(String userName)
	{
		WebElement heading = _wait.until(ExpectedConditions.visibilityOfElementLocated(_header));
		assertEquals("MY ACCOUNT", heading.getText());
        assertEquals(userName, driver.findElement(_accountName).getText());
        assertTrue(_driver.findElement(_infoAccount).getText().contains("Welcome to your account."));
        assertTrue(_driver.findElement(_logOut).isDisplayed());
        assertTrue(_driver.getCurrentUrl().contains("controller=my-account"));
	}
}