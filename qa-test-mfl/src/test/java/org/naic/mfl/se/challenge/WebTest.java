package org.naic.mfl.se.challenge;

import org.junit.Before;
import org.junit.Test;
import org.testng;
import org.testng.annotations.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import PageObject.Login;
import PageObject.CreateAccount;
import PageObject.WomenCategory;
import PageObject.Account;
import PageObject.Product;
import PageObject.Order;

import java.util.Date;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;

public class WebTest {
    WebDriver driver;
    WebDriverWait wait;

    String existingUserEmail = "mflsqe@naic.org";
    String existingUserPassword = "mflsqe1234";

    @BeforeTest
    public void setUp() {
        System.setProperty("webdriver.chrome.driver", "src/test/resources/chromedriver.exe");
        driver = new ChromeDriver();
        wait = new WebDriverWait(driver, 10, 50);
        driver.get("http://automationpractice.com/index.php");
    }

    @Test
    public void signInTest() {
        Login login = new Login(driver, wait);
        CreateAccount createAccount = new CreateAccount(driver, wait);
        
        //CLick SignIn Button
        login.ClickLogin();

        //Create Account
        String timestamp = String.valueOf(new Date().getTime());
        String email = "hf_challenge_" + timestamp + "@hf" + timestamp.substring(7) + ".com";
        String name = "Firstname";
        String surname = "Lastname";
        String password = "Qwerty";
        CreateAccount.CreateNewUserAccount(email, name, surname, password);

        //Verify Login Successful
        Login.VerifyLogin(name + " " + surname);
    }

    @Test
    public void logInTest() {
        Login login = new Login(driver, wait);      
        String fullName = "Joe Black";

        //CLick SignIn Button
        login.ClickLogin();     

        //Sign into Site
        Login.SignIntoSite(existingUserEmail, existingUserPassword);

        //Verify Login
        Login.VerifyLogin(fullName);
    }

    @Test
    public void checkoutTest() {
        Login login = new Login(driver, wait);
        Account account = new Account(driver, wait);
        WomenCategory womenCategory = new WomenCategory(driver, wait);
        Product product = new Product(driver, wait);
        Order order = new Order(driver, wait);

        //Click SignIN Button
        Login.ClickLogin();

        //Sign Into Site
        Login.SignIntoSite(existingUserEmail, existingUserPassword);

        //Select Women Link
        account.ClickWomenLink();

        //Select first item
        womenCategory.SelectFirstItem();

        //Click Add To Cart
        Product.SelectAddToCart();

        //Proceed to Checkout
        Product.ProceedToCheckout();

        //Click proceed to checkout / process Address
        order.ProcessAddress();       
        
        //Agree to Terms of Service
        order.AgreeToTermsOfService();

        //Process Carrier
        order.ProcessCarrier();

        //Select Bank Wire
        order.SelectBankWire();
        
        // Confirm order
        order.ConfirmOrder();

        //Verify Order Confirmation Info
        order.VerifyOrderConfirmation()
    }
}
