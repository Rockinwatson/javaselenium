import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

public class Account{

	WebDriver _driver;
	WebDriverWait _wait;

	public Account(WebDriver driver, WebDriverWait wait)
	{
		_driver = driver;
		_wait = wait;
	}

	By _womenLink = By.linkText("Women")

	public void ClickWomenLink()
	{
		_wait.until(ExpectedConditions.visibilityOfElementLocated(_womenLink)).click();
	}
}