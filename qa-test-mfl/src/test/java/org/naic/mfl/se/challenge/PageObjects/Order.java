import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

public class Order{
	WebDriver _driver;
	WebDriverWait _wait;

	public Order(WebDriver driver, WebDriverWait wait)
	{
		_driver = driver;
		_wait = wait;
	}

	By _processAddress = By.name("processAddress");
	By _termsOfServiceCheckBox = By.id("uniform-cgv");
	By _processCarrier = By.name("processCarrier");
	By _bankWire = By.className("bankwire");
	By _confirmOrder = By.xpath("//*[@id='cart_navigation']/button");
	By _orderConfirmHeader = By.cssSelector("h1");
	By _lastStepsComplete = By.xpath("//li[@class='step_done step_done_last four']");
	By _currentStepLastStep = By.xpath("//li[@id='step_end' and @class='step_current last']");
	By _orderCompleteText = By.xpath("//*[@class='cheque-indent']/strong");

	public void ProcessAddress()
	{
		_wait.until(ExpectedConditions.visibilityOfElementLocated(_processAddress)).click();
	}

	public void ProcessCarrier()
	{
		_driver.findElement(_processCarrier).click();
	}

	public void AgreeToTermsOfService()
	{
		_wait.until(ExpectedConditions.visibilityOfElementLocated(_termsOfServiceCheckBox)).click();
	}

	public void SelectBankWire()
	{
		_wait.until(ExpectedConditions.visibilityOfElementLocated(_bankWire)).click();
	}

	public void ConfirmOrder()
	{
		_wait.until(ExpectedConditions.visibilityOfElementLocated(_confirmOrder)).click();
	}

	public void VerifyOrderConfirmation()
	{
		WebElement heading = _wait.until(ExpectedConditions.visibilityOfElementLocated(_orderConfirmHeader));
        assertEquals("ORDER CONFIRMATION", heading.getText());
        assertTrue(driver.findElement(_lastStepsComplete).isDisplayed());
        assertTrue(driver.findElement(_currentStepLastStep).isDisplayed());
        assertTrue(driver.findElement(_orderCompleteText).getText().contains("Your order on My Store is complete."));
        assertTrue(driver.getCurrentUrl().contains("controller=order-confirmation"));
	}
}